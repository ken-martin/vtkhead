/*=========================================================================

  Program:   Visualization Toolkit

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// test baking shadow maps
//
// The command line arguments are:
// -I        => run in interactive mode; unless this is used, the program will
//              not allow interaction and exit

#include "vtkActor.h"
#include "vtkCamera.h"
#include "vtkCameraPass.h"
#include "vtkCellArray.h"
#include "vtkJPEGReader.h"
#include "vtkLight.h"
#include "vtkLightKit.h"
#include "vtkNew.h"
#include "vtkOpenGLPolyDataMapper.h"
#include "vtkOpenGLRenderer.h"
#include "vtkOpenGLTexture.h"
#include "vtkPlaneSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkPolyData.h"
#include "vtkProperty.h"
#include "vtkRenderPassCollection.h"
#include "vtkRenderStepsPass.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkSequencePass.h"
#include "vtkShadowMapBakerPass.h"
#include "vtkShadowMapPass.h"
#include "vtkTextureObject.h"
#include "vtkTimerLog.h"
#include "vtkXMLPolyDataReader.h"
#include "vtkSSAAPass.h"

//----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  vtkNew<vtkRenderer> renderer;
  renderer->SetBackground(0.3, 0.4, 0.6);
  vtkNew<vtkRenderWindow> renderWindow;
  renderWindow->SetSize(600, 600);
  renderWindow->UseSRGBColorSpaceOn();
  renderWindow->AddRenderer(renderer.Get());
  vtkNew<vtkRenderWindowInteractor>  iren;
  iren->SetRenderWindow(renderWindow.Get());

  vtkNew<vtkLight> light1;
  light1->SetFocalPoint(0,0,0);
  //light1->PositionalOn();
  light1->SetPosition(0.3, 1, 0.3);
  light1->SetColor(0.95, 0.97, 1.0);
//  light1->SetColor(1.0, 0.0, 0.0);
  light1->SetIntensity(0.8);
  renderer->AddLight(light1.Get());
  //light1->SetLightTypeToCameraLight();

  vtkNew<vtkLight> light2;
  light2->SetFocalPoint(0, 0, 0);
  light2->SetPosition(0.0, 0.0, 1.0);
  light2->SetColor(1.0, 0.8, 0.7);
  light2->SetIntensity(0.5);
  renderer->AddLight(light2.Get());
  //light2->SetLightTypeToCameraLight();

  vtkNew<vtkLight> light3;
  light3->SetFocalPoint(0, 0, 0);
  light3->SetPosition(-0.3, 0.6, -1.0);
  light3->SetColor(1.0, 0.8, 0.7);
  light3->SetIntensity(0.8);
  renderer->AddLight(light3.Get());
  //light3->SetLightTypeToCameraLight();

  const char* fileName = "c:/Users/ken.martin/Documents/head/head.vtp";
  vtkNew<vtkXMLPolyDataReader> reader;
  reader->SetFileName(fileName);
  reader->Update();

  fileName = "c:/Users/ken.martin/Documents/head/Map-COL.jpg";
  vtkNew<vtkJPEGReader> colorreader;
  colorreader->SetFileName(fileName);
  colorreader->Update();

  vtkNew<vtkTexture> ctex;
  ctex->SetInputConnection(colorreader->GetOutputPort());
  ctex->MipmapOn();
  ctex->UseSRGBColorSpaceOn();
  ctex->InterpolateOn();

  fileName = "c:/Users/ken.martin/Documents/head/Infinite-Level_02_World_NoSmoothUV.jpg";
  vtkNew<vtkJPEGReader> normreader;
  normreader->SetFileName(fileName);
  normreader->Update();
  vtkNew<vtkTexture> ntex;
  ntex->SetInputConnection(normreader->GetOutputPort());
  //ntex->MipmapOn();
  ntex->InterpolateOn();

  vtkNew<vtkOpenGLPolyDataMapper> mapper;
  mapper->SetInputConnection(reader->GetOutputPort());

  // now modify the fragment shader for normal map
  mapper->AddShaderReplacement(
    vtkShader::Fragment,  // in the fragment shader
    "//VTK::Normal::Dec", // replace the normal block
    true, // before the standard replacements
    "uniform mat3 normalMatrix;\n", //but we add this
    false // only do it once
    );
  mapper->AddShaderReplacement(
    vtkShader::Fragment,  // in the fragment shader
    "//VTK::Normal::Impl", // replace the normal block
    true, // before the standard replacements
    "vec3 normalVCVSOutput = \n"
    "  texture(texture_1, tcoordVCVSOutput.st).xyz;\n"
    "normalVCVSOutput = normalVCVSOutput*2.0 - 1.0;\n"
    "normalVCVSOutput = normalize(normalMatrix * normalVCVSOutput);\n",
    false // only do it once
    );

  vtkNew<vtkActor> actor;
  actor->GetProperty()->SetTexture("color", ctex.Get());
  actor->GetProperty()->SetTexture("normal", ntex.Get());
  actor->SetMapper(mapper.Get());
  actor->GetProperty()->SetAmbientColor(0.2, 0.06, 0.0);
  actor->GetProperty()->SetDiffuseColor(0.94, 0.97, 1.0);
  // actor->GetProperty()->SetSpecularColor(1.0, 1.0, 1.0);
  actor->GetProperty()->SetSpecular(0.15);
  actor->GetProperty()->SetDiffuse(1.0);
  //actor->GetProperty()->SetAmbient(1.0);
  actor->GetProperty()->SetSpecularPower(50.0);
  //actor->GetProperty()->SetOpacity(1.0);
  renderer->AddActor(actor.Get());

  renderWindow->SetMultiSamples(0);

  vtkNew<vtkShadowMapPass> shadows;
  shadows->GetShadowMapBakerPass()->SetResolution(2048);

  vtkNew<vtkSequencePass> seq;
  vtkNew<vtkRenderPassCollection> passes;
  passes->AddItem(shadows->GetShadowMapBakerPass());
  passes->AddItem(shadows.Get());
  seq->SetPasses(passes.Get());

  vtkNew<vtkCameraPass> cameraP;
  cameraP->SetDelegatePass(seq.Get());

  // finally blur the resulting image
  // The blur delegates rendering the unblured image
  // to the basicPasses
  vtkNew<vtkSSAAPass> ssaa;
  ssaa->SetDelegatePass(cameraP.Get());

  // tell the renderer to use our render pass pipeline
  vtkOpenGLRenderer *glrenderer =
    vtkOpenGLRenderer::SafeDownCast(renderer.GetPointer());
  //glrenderer->SetPass(cameraP.Get());
  glrenderer->SetPass(ssaa.Get());

  vtkNew<vtkTimerLog> timer;
  timer->StartTimer();
  renderWindow->Render();
  timer->StopTimer();
  double firstRender = timer->GetElapsedTime();
  cerr << "first render time: " << firstRender << endl;

  timer->StartTimer();
  int numRenders = 8;
  for (int i = 0; i < numRenders; ++i)
  {
    renderer->GetActiveCamera()->Azimuth(80.0/numRenders);
    renderer->GetActiveCamera()->Elevation(80.0/numRenders);
    renderWindow->Render();
  }
  timer->StopTimer();
  double elapsed = timer->GetElapsedTime();
  cerr << "interactive render time: " << elapsed / numRenders << endl;
  unsigned int numTris = reader->GetOutput()->GetPolys()->GetNumberOfCells();
  cerr << "number of triangles: " <<  numTris << endl;
  cerr << "triangles per second: " <<  numTris*(numRenders/elapsed) << endl;

#if 0
  vtkNew<vtkShadowMapBakerPass> bakerPass;
  glrenderer->SetPass(bakerPass.Get());
  renderWindow->Render();

  // get a shadow map
  vtkTextureObject *to = (*bakerPass->GetShadowMaps())[0];
  // by default the textures have depth comparison on
  // but for simple display we need to turn it off
  to->SetDepthTextureCompare(false);

  // now render this texture so we can see the depth map
  vtkNew<vtkActor> actor2;
  vtkNew<vtkPolyDataMapper> mapper2;
  vtkNew<vtkOpenGLTexture> texture;
  texture->SetTextureObject(to);
  actor2->SetTexture(texture.Get());
  actor2->SetMapper(mapper2.Get());

  vtkNew<vtkPlaneSource> plane;
  mapper2->SetInputConnection(plane->GetOutputPort());
  renderer->RemoveActor(actor.Get());
  renderer->AddActor(actor2.Get());
  glrenderer->SetPass(nullptr);

#endif

  renderer->GetActiveCamera()->SetPosition(-0.2,0.2,1);
  renderer->GetActiveCamera()->SetFocalPoint(0,0,0);
  renderer->GetActiveCamera()->SetViewUp(0,1,0);
  renderer->GetActiveCamera()->OrthogonalizeViewUp();
  renderer->ResetCamera();
  renderer->GetActiveCamera()->Zoom(2.5);
  renderWindow->Render();

  iren->Start();
}
